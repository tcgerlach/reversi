﻿using UnityEngine;
using System.Collections;

public class Chip : MonoBehaviour {

	// flip the chip over
	public void flip() {
		this.transform.Rotate (new Vector3 (180, 0, 0));
	}

	// register this transform with the game manager when created
	void OnNetworkInstantiate(NetworkMessageInfo info) {
		GameController.Instance.RegisterPiece(this.gameObject);
	}
}
