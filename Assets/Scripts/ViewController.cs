﻿using UnityEngine;
using System.Collections;

public class ViewController : MonoBehaviour {

	// Default position is from an angle
	private Vector3 DefaultPosition = new Vector3 (0f, 134.3f, -126.7f);
	private Vector3 DefaultAngle = new Vector3(55.3279f,0f,0f);

	// But camera can be switched to an overhead view
	private Vector3 TopPosition = new Vector3(0f,180f,0f);
	private Vector3 TopAngle = new Vector3 (90f,0f,0f);

	private bool viewingFromDefault = true;

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.C)) {
			if (viewingFromDefault) {
				this.transform.position = TopPosition;
				this.transform.eulerAngles = TopAngle;
			} else {
				this.transform.position = DefaultPosition;
				this.transform.eulerAngles = DefaultAngle;
			}

			viewingFromDefault = !viewingFromDefault;
		}
	}
}
