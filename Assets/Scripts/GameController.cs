﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

	//*********************************************************************
	// SINGLETON CODE
	private static GameController instance = null;

	// singleton getter
	public static GameController Instance {
		get {
			if (instance == null) {
				GameObject o = new GameObject();
				instance = o.AddComponent<GameController>();			
			}
			return instance;
		}
	}

	// ensure this object is never created elsewhere
	void Awake() {
		if ((instance) && (instance.GetInstanceID() != GetInstanceID())) {
			DestroyImmediate(gameObject);
		} else {
			instance = this;
			DontDestroyOnLoad(gameObject);
		}
	}

	//*********************************************************************
	// HUD CODE
	public Text TurnIndicator;
	public Text BlackScore;
	public Text WhiteScore;
	public Canvas StatusCanvas;
	private bool GameOver = false;

	public void OnGUI() {
		if (!GameOver) {
			if (!TestMode) {
				TurnIndicator.text = myTurn ? "Your Turn!" : "Their Turn";
			} else {
				TurnIndicator.text = flipCount++ % 2 == 0 ? "Black Turn" : "White Turn";
			}
		}
		BlackScore.text = "Black: " + getScore (BLACK);
		WhiteScore.text = "White: " + getScore (WHITE);
	}

	private int getScore(int color) {
		int score = 0;
		for (int i = 0; i < 8; ++i) {
			for (int j = 0; j < 8; ++j) {
				if (board [i, j] == color) {
					++score;
				}
			}
		}
		return score;
	}

	//*********************************************************************
	// CORE UNITY FUNCTIONS
	void Start() {
		// set board to default - no pieces
		for (int i = 0; i < 8; ++i) {
			for (int j = 0; j < 8; ++j) {
				board [i, j] = EMPTY;
			}
		}

		// setup default pieces and variables for test mode
		if (TestMode) {
			MakeTestPiece (3, 4); 
			MakeTestPiece (4, 4); 
			MakeTestPiece (4, 3);
			MakeTestPiece (3, 3); 

			StatusCanvas.enabled = true;
			GameOver = false;				// start now!
			myTurn = true;					// always my turn on test mode
		}
	}

	// Update method - called every frame
	void Update () {

		// Did the user click a spot to place a disc?
		if (Input.GetMouseButtonDown (0)) {

			// find where the user clicked
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			Physics.Raycast(ray, out hit);

			if (!hit.point.Equals (Vector3.zero)) {					// make sure we hit something
				Vector2 grid = CalculateGridFromClick (hit.point);	// convert click to board x/y
				Vector3 loc = CalculateLocationFromGrid (grid);		// convert back to center location for piece

				int x = (int)grid.x;								// extract x & y of board from vector
				int y = (int)grid.y;
				int color = NextPieceColor();						// get color of piece to be played

				// if it's a valid move, place the piece
				if (myTurn && isEmpty (grid) && isFlankingMove(x,y,color)) {
					if (TestMode) {
						// for test mode, make piece and flip board
						MakeTestPiece(x,y);
						flipFlankedDiscs(x, y, color);
					} else {
						// Network.Instantiate will create the piece, then the piece will
						// register itself with this class when it is created
						// server pieces are white
						Quaternion rot = Network.isServer ? WhiteRotation : BlackRotation;
						Network.Instantiate (GamePiece, loc, rot, 1); 
					}
				} 
			}
		}

		// Do we need to dump the board for debuging?
		if (Input.GetKeyDown(KeyCode.D)) {
			DumpBoard();
		}
	}
		
	//*********************************************************************
	// GAME CODE

	// game variables
	public bool TestMode = false;		// If true, no network code is executed
	private int flipCount = 0;			// For test mode only, flips every other chip
	public GameObject GamePiece;		// Prefab for game piece
	private bool myTurn = false;		// is it my turn?

	// easiest to deal with board as an array of integers
	private int[,] board = new int[8,8];
	private const int EMPTY = 0;
	private const int WHITE = 1;
	private const int BLACK = 2;

	// but still need references to the game object so we can flip them
	private GameObject[,] pieces = new GameObject[8, 8];

	// start game when both sides connected - server goes first
	public void StartGame() {
		Debug.Log ("Game starting!");

		// game started - show hud!
		StatusCanvas.enabled = true;

		// create pieces
		pieces[3,4] = Instantiate (GamePiece, CalculateLocationFromGrid (new Vector2 (3, 4)), WhiteRotation) as GameObject; 
		pieces[4,4] = Instantiate (GamePiece, CalculateLocationFromGrid (new Vector2 (4, 4)), BlackRotation) as GameObject; 
		pieces[3,3] = Instantiate (GamePiece, CalculateLocationFromGrid (new Vector2 (3, 3)), BlackRotation) as GameObject; 
		pieces[4,3] = Instantiate (GamePiece, CalculateLocationFromGrid (new Vector2 (4, 3)), WhiteRotation) as GameObject;

		// set board state
		board[3,4] = WHITE;
		board[4,4] = BLACK;
		board[3,3] = BLACK;
		board[4,3] = WHITE;

		// let server player go first
		myTurn = Network.isServer;
	}

	// Position/Rotation information for placed discs
	private readonly static Quaternion WhiteRotation = Quaternion.Euler (new Vector3 (90, 0, 0));
	private readonly static Quaternion BlackRotation = Quaternion.Euler (new Vector3 (-90, 0, 0));
	private const float DEFAULT_Y = .5f;	
		
	// THIS IS ONLY CALLED WHEN IN NETWORKED MODE
	// when network pieces are created, they are registered here
	// this will ensure that both sides have the same state
	public void RegisterPiece(GameObject chip) {
		Vector2 move = CalculateGridFromClick (chip.transform.position);
		int x = (int)move.x;
		int y = (int)move.y;
		Debug.Log ("Received Move:" + x + ", " + y);
		pieces[x,y] = chip;

		// if this is server side and it is server player turn - white
		// if it is client side and client player turn - black
		int chipColor;
		if (Network.isServer) {
			chipColor = myTurn ? WHITE : BLACK;
		} else {
			chipColor = myTurn ? BLACK : WHITE;
		}

		board [x, y] = chipColor;
			
		// flip pieces
		flipFlankedDiscs(x, y, board[x,y]);

		// is game over?
		if (isGameOver ()) {
			GameOver = true;
			// TODO restart button
			TurnIndicator.text = "Game Over";
		} else {
			// does opponent have a move?
			if (hasMove (getOppositeDisc (chipColor))) {
				// switch turn
				myTurn = !myTurn;
			} else {
				// TODO notify both players of this
				Debug.Log ("Current player gets extra turn");
			}
		}
	}

	// THIS IS ONLY CALLED IN TEST MODE
	// for local testing, switch back and forth between white and black chips
	private void MakeTestPiece(int x, int y) {
		GameObject piece = Instantiate (GamePiece, CalculateLocationFromGrid(new Vector2(x,y)), BlackRotation) as GameObject;
		pieces [x, y] = piece;

		int currentColor;

		// every other chip is black
		if (flipCount++ % 2 == 0) {
			piece.GetComponent<Chip> ().flip ();
			currentColor = WHITE;
		} else {
			currentColor = BLACK;
		}
		board [x, y] = currentColor;

		// ignore this during initial setup
		if (flipCount > 4) {
			// end game if no moves
			if (isGameOver ()) {
				GameOver = true;
				TurnIndicator.text = "Game Over";
			}

			// see if we can change to the white player
			if (!hasMove (WHITE) && currentColor == BLACK) {
				flipCount--;	// extra turn!
				Debug.Log ("White has no move, black goes again");
			}

			// see if we can change to the black player
			if (!hasMove (BLACK) && currentColor == WHITE) {
				flipCount--;	// extra turn!
				Debug.Log ("Black has no move, white goes again");
			}
		}
	}

	// determine piece of next disc to be played
	private int NextPieceColor() {
		if (TestMode) {
			return flipCount % 2 == 0 ? WHITE : BLACK;
		} else {
			return  Network.isServer ? WHITE : BLACK;
		}
	}

	// returns true if a particular space on the board is empty
	private bool isEmpty(Vector2 grid) {
		return board [(int)grid.x, (int)grid.y] == EMPTY;
	}
		
	//*********************************************************************
	// CODE FOR BOARD POSITION CALCULATIONS

	// the coordinates for each square of board - board centered on 0,0, so these are the same for X and Y
	private static readonly float[] POSITIONS = new float[] {-75f,-53.5f,-32f,-10.5f,10.5f,32f,53.5f,75f};
		
	// determine what grid item was clicked
	public static Vector2 CalculateGridFromClick(Vector3 click) {
		int col = GetNearestIndex (click.x);
		int row = GetNearestIndex (click.z);
		return new Vector2 (col, row);
	}

	// find the nearest position to the point 
	private static int GetNearestIndex(float point) {
		float diff = 999;
		int index = -1;
		for(int i = 0; i < 8; ++i) {
			float currDiff = Mathf.Abs(POSITIONS[i] - point);
			if (currDiff < diff) {
				index = i;
				diff = currDiff;
			}
		}
		return index;
	}

	// given the piece grid number, return the location to draw the piece
	public static Vector3 CalculateLocationFromGrid(Vector2 grid) {
		float x = POSITIONS [(int)grid.x];
		float z = POSITIONS [(int)grid.y];
		return new Vector3 (x, DEFAULT_Y, z);
	}

	//*********************************************************************
	// BASIC FUNCTION FOR DEBUGGING BOARD
	private void DumpBoard() {
		string output;
		for (int row = 7; row >= 0; --row) {
			output = ""; 
			for (int col = 0; col < 8; ++col) {
				output += board [col, row] + "|";
			}
			Debug.Log (output);
		}
	}

	//*********************************************************************
	//	VIC'S CODE FOR GAME LOGIC
	// check every direction for flanking moves and flip
	private void flipFlankedDiscs(int x, int y, int disc) {
		tryToFlipFlankedDiscs(x, y, -1, -1, disc);
		tryToFlipFlankedDiscs(x, y,  0, -1, disc);
		tryToFlipFlankedDiscs(x, y,  1, -1, disc);

		tryToFlipFlankedDiscs(x, y, -1,  0, disc);
		tryToFlipFlankedDiscs(x, y,  1,  0, disc);

		tryToFlipFlankedDiscs(x, y, -1,  1, disc);
		tryToFlipFlankedDiscs(x, y,  0,  1, disc);
		tryToFlipFlankedDiscs(x, y,  1,  1, disc);
	}

	// check direction x and direction y for flaking move
	// if it exists, flip discs
	private void tryToFlipFlankedDiscs(int x, int y, int dx, int dy, int disc) {
		if (isFlankingMove(x, y, dx, dy, disc)) {
			flipFlankedDiscs(x, y, dx, dy, disc);
		}
	}

	// flip the disks in the given direction - dx * dy
	private void flipFlankedDiscs(int x, int y, int dx, int dy, int disc) {
		while (true) {
			x += dx;
			y += dy;

			int current = board[x, y];

			if (disc == current) {
				break;
			}

			flipDisc(x, y);
		}
	}

	// flip an individual disc
	private void flipDisc(int x, int y) {
		board[x, y] = getOppositeDisc(board[x, y]);
		pieces [x, y].GetComponent<Chip> ().flip ();
	}

	// check to see if the click is a legit move
	private bool isFlankingMove(int x, int y, int disc) {
		return 
			isFlankingMove(x, y, -1, -1, disc) ||
			isFlankingMove(x, y,  0, -1, disc) ||
			isFlankingMove(x, y,  1, -1, disc) ||

			isFlankingMove(x, y, -1,  0, disc) ||
			isFlankingMove(x, y,  1,  0, disc) ||

			isFlankingMove(x, y, -1,  1, disc) ||
			isFlankingMove(x, y,  0,  1, disc) ||
			isFlankingMove(x, y,  1,  1, disc);
	}

	private bool isFlankingMove(int x, int y, int dx, int dy, int disc) {
		int opponent = getOppositeDisc(disc);

		bool valid = true;
		int result = 0;
		int d;
		do {
			x += dx;
			y += dy;

			if (((x | y) & 0x08) == 0x08) {
				// Off the board
				valid = false;
				break;
			}

			d = board[x, y];
			result |= (int)d;
		} while (d == opponent);

		valid = valid && result == 3;

		return valid;
	}

	private int getOppositeDisc(int disc) {
		return disc == WHITE ? BLACK : WHITE;
	}

	//*********************************************************************
	//	LOGIC FOR END OF GAME
	private bool hasMove(int color) {
		// check through board
		// if an empty space is found, see if it is a valid move
		// as soon as a move is found, return true
		// if no move found, return false
		for (int i = 0; i < 8; ++i) {
			for (int j = 0; j < 8; ++j) {
				if (board [i, j] == EMPTY) {
					if (isFlankingMove(i,j,color)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	private bool isGameOver() {
		return !hasMove (BLACK) && !hasMove (WHITE);
	}
}
