﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class NetworkSetup : MonoBehaviour {

	// default server port
	public int Port = 25005;				

	// connection info for client
	private string ServerIP = "127.0.0.1";			
	private string ServerPort = "25005";

	// keep track of which gui is running
	private bool showSetup = true;	
	private bool showServer = false;
	private bool showClient = false;

	// network status string
	private string status;

	private bool TestMode = false;

	// if in test mode, don't show network setup
	void Start() {
		if (GetComponent<GameController> ().TestMode) {
			TestMode = true;
		}
	}

	// show the current gui state
	void OnGUI() {
		// no network gui on test mode
		if (TestMode) {
			return;
		}

		if (showSetup) {
			SetupGui ();
		} else if (showServer) {
			ServerGui ();
		} else if (showClient) {
			ClientGui ();
		} else {
			GUILayout.Label (status);
		}
	}

	// gui to allow for client or server setup
	void SetupGui() {
		if( GUILayout.Button( "Server Setup" )) {
			showSetup = false;
			showServer = true;
		}

		if( GUILayout.Button( "Client Setup" )) {
			showSetup = false;
			showClient = true;
		}
	}
		
	// gui to start server
	void ServerGui() {
		if( GUILayout.Button( "Launch Server" )) {
			LaunchServer();
			showServer = false;
		}
			
		if (GUILayout.Button ("Cancel")) {
			showSetup = true;
			showServer = false;
		}
	}

	// gui to run as a client
	void ClientGui() {
		GUILayout.Label( "IP Address" );
		ServerIP = GUILayout.TextField( ServerIP, GUILayout.Width( 200f ) );

		GUILayout.Label( "Port" );
		ServerPort = GUILayout.TextField( ServerPort, GUILayout.Width( 50f ) );

		if( GUILayout.Button( "Connect" ) ) {
			LaunchClient ();
			showClient = false;
		}

		if (GUILayout.Button ("Cancel")) {
			showSetup = true;
			showClient = false;
		}
	}

	//*****************************************************
	//				Init Client/Server Functions
	//*****************************************************
	// Start server 
	void LaunchServer() {
		Network.InitializeServer(1, Port, true);
	}

	// Start client
	void LaunchClient() {
		int portNum = 25005;

		if(!int.TryParse( ServerPort, out portNum ) ) {
			// failed to parse number
			Debug.LogWarning( "Given port is not a number" );
		} else {
			// try to initiate a direct connection to the server
			Network.Connect( ServerIP, portNum, "" );
		}
	}

	//*****************************************************
	//					Client Side Functions
	//*****************************************************
	// When connected to server, start game
	void OnConnectedToServer() {
		status = "Connected to server!" ;
		GetComponent<GameController> ().StartGame ();
	}

	void OnDisconnectedFromServer(NetworkDisconnection cause) {
		showSetup = true;
		SceneManager.LoadScene (1);
	}

	// if failed to connect, show setup again and allow user to retry
	void OnFailedToConnect( NetworkConnectionError error ) {
		showSetup = true;
	}
		
	//*****************************************************
	//					Server Side Functions
	//*****************************************************
	// When server initialized, let user know
	void OnServerInitialized() {
		status = "Server initialized";
	}

	void OnPlayerConnected(NetworkPlayer player) {
		status = "Opponent connected";
		GetComponent<GameController> ().StartGame ();
	}

	void OnPlayerDisconnected(NetworkPlayer player) {
		showSetup = true;
		SceneManager.LoadScene (1);
	}
}
