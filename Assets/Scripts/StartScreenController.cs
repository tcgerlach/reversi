﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class StartScreenController : MonoBehaviour {

	public Canvas Credits;
	public Canvas StartScreen;

	public void ShowCredits() {
		StartScreen.enabled = false;
		Credits.enabled = true;
	}

	public void HideCredits() {
		StartScreen.enabled = true;
		Credits.enabled = false;
	}

	public void StartGame() {
		SceneManager.LoadScene (1);
	}
}
